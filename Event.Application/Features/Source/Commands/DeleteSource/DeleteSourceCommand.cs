﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Source.Commands.DeleteSource
{
    public class DeleteSourceCommand : IRequest
    {
        public Guid SourceId { get; set; }
    }
}

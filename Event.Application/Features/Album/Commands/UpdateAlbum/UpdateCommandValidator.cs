﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Album.Commands.UpdateAlbum
{
    public class UpdateCommandValidator : AbstractValidator<UpdateAlbumCommand>
    {
        public UpdateCommandValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);


        }
    }
}

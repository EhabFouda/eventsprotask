﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Album.Queries.GetAlbumList
{
    public class GetAlbumListQuery : IRequest<List<GetAlbumListViewModel>>
    {
        public Guid EventId { get; set; }

    }
}

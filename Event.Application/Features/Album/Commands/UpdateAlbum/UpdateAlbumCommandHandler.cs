﻿using AutoMapper;
using Event.Application.Contracts;
using Event.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Album.Commands.UpdateAlbum
{
    public class UpdateAlbumCommandHandler : IRequestHandler<UpdateAlbumCommand>
    {
        private readonly IAsyncRepository<DAlbum> _albumRepository;
        private readonly IMapper _mapper;

        public UpdateAlbumCommandHandler(IAsyncRepository<DAlbum> albumRepository, IMapper mapper)
        {
            _albumRepository = albumRepository;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateAlbumCommand request, CancellationToken cancellationToken)
        {
            DAlbum album = _mapper.Map<DAlbum>(request);
            await _albumRepository.UpdateAsync(album);

            return Unit.Value;
        }
    }
}

﻿using AutoMapper;
using Event.Application.Contracts;
using Event.Application.Features.Event.Queries.GetEventList;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Event.Queries.GetPostsList
{
    public class GetEventListQueryHandler : IRequestHandler<GetEventListQuery, List<GetEventListViewModel>>
    {
        private readonly IEventRepository _eventRepository;
        private readonly IMapper _mapper;

        public GetEventListQueryHandler(IEventRepository eventRepository, IMapper mapper)
        {
            _eventRepository = eventRepository;
            _mapper = mapper;
        }

        public async Task<List<GetEventListViewModel>> Handle(GetEventListQuery request, CancellationToken cancellationToken)
        {
            var allevent = await _eventRepository.ListAllAsync();
            return _mapper.Map<List<GetEventListViewModel>>(allevent);
        }
    }
}

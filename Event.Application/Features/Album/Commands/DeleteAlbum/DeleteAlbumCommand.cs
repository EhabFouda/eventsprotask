﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Album.Commands.DeleteAlbum
{
    public class DeleteAlbumCommand : IRequest
    {
        public Guid AlbumId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Source.Queries.GetSourceDetails
{
    public class GetSourceDetailsViewModel
    {
        public Guid Id { get; set; }
        public Guid? EventId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}

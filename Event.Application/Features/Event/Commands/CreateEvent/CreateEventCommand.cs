﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Event.Commands.CreateEvent
{
    [ValidateNever]
    public class CreateEventCommand : IRequest<Guid>
    {
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public string Content { get; set; }
        public string Address { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public IFormFile CoverImg { get; set; }

    }
}

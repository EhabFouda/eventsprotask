﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Source.Commands.CreateSource
{
    public class EventSelectedViewModel
    {
        public Guid EventId { get; set; }
        public string Name { get; set; }
    }
}

﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Category.Queries.GetCategoryDetails
{
    public class GetCategoryDetailsQuery : IRequest<GetCatgoryDetailsViewModel>
    {
        public Guid CategoryId { get; set; }
    }
}

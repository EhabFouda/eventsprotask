﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Common.Helper
{
    public interface IHelper
    {
        public string Upload(IFormFile Photo);
    }
}

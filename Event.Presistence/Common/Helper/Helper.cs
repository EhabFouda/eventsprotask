﻿using Event.Application.Common.Helper;
using Microsoft.AspNetCore.Http;
using System;
using DinkToPdf;
using DinkToPdf.Contracts;
using System.Collections.Generic;
using Microsoft.AspNetCore.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Presistence.Common.Helper
{
    public class Helper : IHelper
    {
        private readonly IWebHostEnvironment HostingEnvironment;

        public Helper( IWebHostEnvironment hostingEnvironment)
        {
            HostingEnvironment = hostingEnvironment;
        }

        public string Upload(IFormFile Photo)
        {
            string folderName = "Event";
            string uniqueFileName = string.Empty;
            if (Photo != null)
            {
                string uploadsFolder = Path.Combine(HostingEnvironment.WebRootPath, $"images/{folderName}");
                if (!Directory.Exists(uploadsFolder))
                {
                    Directory.CreateDirectory(uploadsFolder);
                }

                uniqueFileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(Photo.FileName);
                string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    Photo.CopyTo(fileStream);
                }
            }
            return $"images/{folderName}/" + uniqueFileName;
        }

    }
}

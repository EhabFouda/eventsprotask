﻿using Event.Domain.Entities;

namespace Event.Application.Contracts
{
    public interface ICategoryRepository : IAsyncRepository<DCategory>
    {
        Task<List<DCategory>> GetListCategoriesByEventIdAsync(Guid id);
    }
}

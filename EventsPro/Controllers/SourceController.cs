﻿using Event.Application.Features.Event.Queries.GetPostsList;
using Event.Application.Features.Source.Commands.CreateSource;
using Event.Application.Features.Source.Commands.DeleteSource;
using Event.Application.Features.Source.Commands.UpdateSource;
using Event.Application.Features.Source.Queries.GetSourceDetails;
using Event.Application.Features.Source.Queries.GetSourceList;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Event.Web.Controllers
{
    public class SourceController : Controller
    {
        private readonly IMediator _mediator;

        public SourceController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<List<GetSourceListViewModel>>> GetListSources()
        {
            return View(await _mediator.Send(new GetSourceListQuery()));
        }

        [HttpGet]
        public async Task<ActionResult<GetSourceDetailsViewModel>> GetEventById(Guid Id)
        {
            return View(await _mediator.Send(new GetSourceDetailsQuery() { SourceId = Id }));
        }

        [HttpGet]
        public async Task<ActionResult> CreateSource()
        {
           var data = await _mediator.Send(new GetEventListQuery());

            ViewBag.Event = data.Select(c => new EventSelectedViewModel
            {
                    EventId = c.Id,
                    Name = c.NameAr
                });
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateSource([FromForm] CreateSourceCommand createSourceCommand)
        {
            Guid id = await _mediator.Send(createSourceCommand);

            return RedirectToAction(nameof(GetListSources));
        }

        [HttpGet]
        public async Task<ActionResult<GetSourceDetailsViewModel>> UpdateSource(Guid Id)
        {
            return View(await _mediator.Send(new GetSourceDetailsQuery() { SourceId = Id }));
        }
        [HttpPost]
        public async Task<ActionResult> UpdateSource([FromForm] UpdateSourceCommand updateEventCommand)
        {
            await _mediator.Send(updateEventCommand);

            return RedirectToAction(nameof(GetListSources));
        }

        [HttpPost]
        public async Task<ActionResult> DeleteSource(Guid id)
        {
            await _mediator.Send(new DeleteSourceCommand() { SourceId = id });

            return Ok(new { key = 1 });
        }
    }
}

﻿using Event.Application.Contracts;
using Event.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Presistence.Repositories
{
    public class SourceRepository : BaseRepository<DSource>, ISourceRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public SourceRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

    }
}

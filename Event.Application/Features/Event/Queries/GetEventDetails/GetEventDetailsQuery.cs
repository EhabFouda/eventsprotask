﻿using Event.Application.Features.Event.Queries.GetEventDetails;
using Event.Application.Features.Event.Queries.GetEventList;
using Event.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Event.Queries.GetPostDetails
{
    public class GetEventDetailsQuery : IRequest<GetEventDetailsViewModel>
    {
        public Guid EventId { get; set; }
    }
}

﻿using AutoMapper;
using Event.Application.Common.Helper;
using Event.Application.Contracts;
using Event.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Images.Commands.UpdateImages
{
    public class UpdateImagesCommandHandler : IRequestHandler<UpdateImagesCommand>
    {
        private readonly IAsyncRepository<DImages> _imagesRepository;
        private readonly IMapper _mapper;
        private readonly IHelper _helper;


        public UpdateImagesCommandHandler(IAsyncRepository<DImages> imageRepository, IMapper mapper, IHelper helper)
        {
            _imagesRepository = imageRepository;
            _mapper = mapper;
            _helper = helper;
        }

        public async Task<Unit> Handle(UpdateImagesCommand request, CancellationToken cancellationToken)
        {
            DImages image = _mapper.Map<DImages>(request);
            image.Image = request.ImageFile != null ? _helper.Upload(request.ImageFile) : image.Image;
            await _imagesRepository.UpdateAsync(image);

            return Unit.Value;
        }
    }
}
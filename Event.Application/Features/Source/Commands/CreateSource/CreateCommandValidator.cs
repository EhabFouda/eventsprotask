﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Source.Commands.CreateSource
{
    internal class CreateCommandValidator : AbstractValidator<CreateSourceCommand>
    {
        public CreateCommandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);

            RuleFor(x => x.Description)
              .NotEmpty()
              .NotNull()
              .MaximumLength(200);
            
            RuleFor(x => x.EventId)
              .NotEmpty()
              .NotNull();


        }
    }
}


﻿using AutoMapper;
using Event.Application.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Album.Queries.GetAlbumList
{
    public class GetAlbumListQueryHandler : IRequestHandler<GetAlbumListQuery, List<GetAlbumListViewModel>>
    {
        private readonly IAlbumRepository _albumRepository;
        private readonly IMapper _mapper;

        public GetAlbumListQueryHandler(IAlbumRepository albumRepository, IMapper mapper)
        {
            _albumRepository = albumRepository;
            _mapper = mapper;
        }

        public async Task<List<GetAlbumListViewModel>> Handle(GetAlbumListQuery request, CancellationToken cancellationToken)
        {
            var allalbum = await _albumRepository.GetListAlbumsByEventIdAsync(request.EventId);
            return _mapper.Map<List<GetAlbumListViewModel>>(allalbum);
        }
    }
}

﻿using AutoMapper;
using Event.Application.Contracts;
using Event.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Album.Commands.CreateAlbum
{
    public class CreateAlbumCommendHandler : IRequestHandler<CreateAlbumCommand, Guid>
    {
        private readonly IAlbumRepository _albumRepository;
        private readonly IMapper _mapper;

        public CreateAlbumCommendHandler(IAlbumRepository albumRepository, IMapper mapper)
        {
            _albumRepository = albumRepository;
            _mapper = mapper;
        }

        public async Task<Guid> Handle(CreateAlbumCommand requset, CancellationToken cancellationToken)
        {
            DAlbum album = _mapper.Map<DAlbum>(requset);

            CreateCommandValidator validator = new CreateCommandValidator();
            var result = await validator.ValidateAsync(requset);
            if (result.Errors.Any())
            {
                throw new Exception("Album is not valid");
            }

            album = await _albumRepository.AddAsync(album);
            return album.Id;
        }
    }
}

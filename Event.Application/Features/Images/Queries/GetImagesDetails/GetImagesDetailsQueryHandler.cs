﻿using AutoMapper;
using Event.Application.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Images.Queries.GetImagesDetails
{
    public class GetImagesDetailsQueryHandler : IRequestHandler<GetImagesDetailsQuery, GetImagesDetailsViewModel>
    {
        private readonly IImageRepository _imagesRepository;

        public GetImagesDetailsQueryHandler(IImageRepository imagesRepository)
        {
            _imagesRepository = imagesRepository;
        }

        public async Task<GetImagesDetailsViewModel> Handle(GetImagesDetailsQuery requset, CancellationToken cancellationToken)
        {
            var detailalbum = await _imagesRepository.GetByIdAsync(requset.ImageId);
            GetImagesDetailsViewModel model = new GetImagesDetailsViewModel()
            {
                Id = detailalbum.Id,
                Image = detailalbum.Image,
                AlbumId = detailalbum.AlbumId,
            };
            return model;
        }
    }
}

﻿using AutoMapper;
using Event.Application.Contracts;
using Event.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Category.Commands.CreateCategory
{
    public class CreateCategoryCommendHandler : IRequestHandler<CreateCategoryCommand, Guid>
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;

        public CreateCategoryCommendHandler(ICategoryRepository categoryRepository, IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public async Task<Guid> Handle(CreateCategoryCommand requset, CancellationToken cancellationToken)
        {
            DCategory category = _mapper.Map<DCategory>(requset);

            CreateCommandValidator validator = new CreateCommandValidator();
            var result = await validator.ValidateAsync(requset);
            if (result.Errors.Any())
            {
                throw new Exception("Category is not valid");
            }

            category = await _categoryRepository.AddAsync(category);
            return category.Id;
        }
    }
}

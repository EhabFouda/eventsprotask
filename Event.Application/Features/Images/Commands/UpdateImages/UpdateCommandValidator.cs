﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Images.Commands.UpdateImages
{
    public class UpdateCommandValidator : AbstractValidator<UpdateImagesCommand>
    {
        public UpdateCommandValidator()
        {
            RuleFor(x => x.AlbumId)
                .NotEmpty()
                .NotNull();


        }
    }
}

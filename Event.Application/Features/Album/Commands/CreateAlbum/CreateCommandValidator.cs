﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Album.Commands.CreateAlbum
{
    public class CreateCommandValidator : AbstractValidator<CreateAlbumCommand>
    {
        public CreateCommandValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);


        }
    }
}

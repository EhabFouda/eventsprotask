﻿using AutoMapper;
using Event.Application.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Source.Queries.GetSourceList
{
    public class GetSourceListQueryHandler : IRequestHandler<GetSourceListQuery, List<GetSourceListViewModel>>
    {
        private readonly ISourceRepository _sourceRepository;
        private readonly IMapper _mapper;

        public GetSourceListQueryHandler(ISourceRepository eventRepository, IMapper mapper)
        {
            _sourceRepository = eventRepository;
            _mapper = mapper;
        }

        public async Task<List<GetSourceListViewModel>> Handle(GetSourceListQuery request, CancellationToken cancellationToken)
        {
            var allevent = await _sourceRepository.ListAllAsync();
            return _mapper.Map<List<GetSourceListViewModel>>(allevent);
        }
    }
}

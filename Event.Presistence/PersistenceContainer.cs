﻿using Event.Application.Common.Helper;
using Event.Application.Contracts;
using Event.Presistence.Common.Helper;
using Event.Presistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Presistence
{
    public static class PersistenceContainer
    {
        public static IServiceCollection AddPersistenceServices(this IServiceCollection services)
        {
            //services.AddDbContext<ApplicationDbContext>(options =>
            //options.UseSqlServer(configuration.GetConnectionString("PostConnectionString")));

            services.AddScoped(typeof(IAsyncRepository<>), typeof(BaseRepository<>));
            services.AddScoped(typeof(IEventRepository), typeof(EventRepository));
            services.AddScoped(typeof(ICategoryRepository), typeof(CategoryRepository));
            services.AddScoped(typeof(IAlbumRepository), typeof(AlbumRepository));
            services.AddScoped(typeof(IImageRepository), typeof(ImagesRepository));
            services.AddScoped(typeof(ISourceRepository), typeof(SourceRepository));
            services.AddScoped(typeof(IHelper), typeof(Helper));

            return services;
        }


       
    }
}

﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Album.Commands.CreateAlbum
{
    public class CreateAlbumCommand : IRequest<Guid>
    {
        public Guid EventId { get; set; }
        public string Title { get; set; }

    }
}

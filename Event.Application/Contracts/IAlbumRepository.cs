﻿using Event.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Contracts
{
    public interface IAlbumRepository : IAsyncRepository<DAlbum>
    {
        Task<List<DAlbum>> GetListAlbumsByEventIdAsync(Guid id);
    }
}

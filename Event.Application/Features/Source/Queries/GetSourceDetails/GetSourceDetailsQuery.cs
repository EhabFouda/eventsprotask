﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Source.Queries.GetSourceDetails
{
    public class GetSourceDetailsQuery : IRequest<GetSourceDetailsViewModel>
    {
        public Guid SourceId { get; set; }
    }
}

﻿using Event.Application.Contracts;
using Event.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Presistence.Repositories
{
    public class ImagesRepository : BaseRepository<DImages>, IImageRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public ImagesRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<DImages>> GetListImagesByAlbumIdAsync(Guid id)
        {
            return (await _dbContext.Images.Where(x => x.AlbumId == id).ToListAsync());
        }
    }
}

﻿using Event.Application.Contracts;
using Event.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Event.Presistence.Repositories
{
    public class CategoryRepository : BaseRepository<DCategory>, ICategoryRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public CategoryRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<DCategory>> GetListCategoriesByEventIdAsync(Guid id)
        {
           return (await _dbContext.Categories.Where(x => x.EventId == id).ToListAsync());
        }
    }
}

﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Category.Queries.GetCategoryList
{
    public class GetCategoryListQuery : IRequest<List<GetCategoryListViewModel>>
    {
        public Guid EventId { get; set; }
    }
}

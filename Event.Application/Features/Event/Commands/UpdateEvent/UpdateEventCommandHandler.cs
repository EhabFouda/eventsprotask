﻿using AutoMapper;
using Event.Application.Common.Helper;
using Event.Application.Contracts;
using Event.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Event.Commands.UpdateEvent
{
    public class UpdateEventCommandHandler : IRequestHandler<UpdateEventCommand>
    {
        private readonly IAsyncRepository<DEvent> _eventRepository;
        private readonly IMapper _mapper;
        private readonly IHelper _helper;

        public UpdateEventCommandHandler(IAsyncRepository<DEvent> eventRepository, IMapper mapper, IHelper helper)
        {
            _eventRepository = eventRepository;
            _mapper = mapper;
            _helper = helper;
        }

        public async Task<Unit> Handle(UpdateEventCommand request, CancellationToken cancellationToken)
        {
            DEvent eventt = _mapper.Map<DEvent>(request);
            eventt.CoverImg = request.ImageFile != null ? _helper.Upload(request.ImageFile) : eventt.CoverImg;
            await _eventRepository.UpdateAsync(eventt);

            return Unit.Value;
        }
    }
}

﻿using AutoMapper;
using Event.Application.Contracts;
using Event.Application.Features.Event.Queries.GetEventDetails;
using Event.Application.Features.Event.Queries.GetEventList;
using Event.Domain.Entities;
using MediatR;

namespace Event.Application.Features.Event.Queries.GetPostDetails
{
    public class GetEventDetailsQueryHandler : IRequestHandler<GetEventDetailsQuery, GetEventDetailsViewModel>
    {
        private readonly IEventRepository _eventRepository;

        public GetEventDetailsQueryHandler(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public async Task<GetEventDetailsViewModel> Handle(GetEventDetailsQuery requset, CancellationToken cancellationToken)
        {
            var detailevent = await _eventRepository.GetByIdAsync(requset.EventId);
            GetEventDetailsViewModel model = new GetEventDetailsViewModel()
            {
                Address = detailevent.Address,
                Content = detailevent.Content,
                CoverImg = detailevent.CoverImg,
                EndDate = detailevent.EndDate,
                Id = detailevent.Id,
                NameAr = detailevent.NameAr,
                NameEn = detailevent.NameEn,
                StartDate = detailevent.StartDate
            };
            return model;
        }
    }
}

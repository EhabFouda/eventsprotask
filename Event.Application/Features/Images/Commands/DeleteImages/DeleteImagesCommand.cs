﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Images.Commands.DeleteImages
{
    public class DeleteImagesCommand : IRequest
    {
        public Guid ImageId { get; set; }
    }
}

﻿using Event.Domain.Entities;

namespace Event.Application.Contracts
{
    public interface ISourceRepository : IAsyncRepository<DSource>
    {
    }
}

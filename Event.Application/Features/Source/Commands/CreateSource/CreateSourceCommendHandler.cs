﻿using AutoMapper;
using Event.Application.Contracts;
using Event.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Source.Commands.CreateSource
{
    internal class CreateSourceCommendHandler : IRequestHandler<CreateSourceCommand, Guid>
    {
        private readonly ISourceRepository _postRepository;
        private readonly IMapper _mapper;

        public CreateSourceCommendHandler(ISourceRepository eventRepository, IMapper mapper)
        {
            _postRepository = eventRepository;
            _mapper = mapper;
        }

        public async Task<Guid> Handle(CreateSourceCommand requset, CancellationToken cancellationToken)
        {
            DSource source = _mapper.Map<DSource>(requset);

            CreateCommandValidator validator = new CreateCommandValidator();
            var result = await validator.ValidateAsync(requset);
            if (result.Errors.Any())
            {
                throw new Exception("Source is not valid");
            }

            source = await _postRepository.AddAsync(source);
            return source.Id;
        }
    }
}
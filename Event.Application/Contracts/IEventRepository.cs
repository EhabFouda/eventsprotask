﻿using Event.Application.Features.Category.Queries.GetCategoryList;
using Event.Application.Features.Event.Queries.GetEventDetails;
using Event.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Contracts
{ 
    public interface IEventRepository : IAsyncRepository<DEvent>
    {

    }
}

﻿using AutoMapper;
using Event.Application.Contracts;
using Event.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Source.Commands.UpdateSource
{
    public class UpdateSourceCommandHandler : IRequestHandler<UpdateSourceCommand>
    {
        private readonly IAsyncRepository<DSource> _sourceRepository;
        private readonly IMapper _mapper;

        public UpdateSourceCommandHandler(IAsyncRepository<DSource> sourceRepository, IMapper mapper)
        {
            _sourceRepository = sourceRepository;
            _mapper = mapper;
        }

        public async Task<Unit> Handle(UpdateSourceCommand request, CancellationToken cancellationToken)
        {
            DSource source = _mapper.Map<DSource>(request);
            await _sourceRepository.UpdateAsync(source);

            return Unit.Value;
        }
    }
}


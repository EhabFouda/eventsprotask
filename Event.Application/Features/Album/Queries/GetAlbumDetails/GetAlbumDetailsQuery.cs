﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Album.Queries.GetAlbumDetails
{
    public class GetAlbumDetailsQuery : IRequest<GetAlbumDetailsViewModel>
    {
        public Guid AlbumId { get; set; }
    }
}

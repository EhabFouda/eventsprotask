﻿using AutoMapper;
using Event.Application.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Category.Queries.GetCategoryDetails
{
    public class GetCategoryDetailsQueryHandler : IRequestHandler<GetCategoryDetailsQuery, GetCatgoryDetailsViewModel>
    {
        private readonly ICategoryRepository _categoryRepository;

        public GetCategoryDetailsQueryHandler(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public async Task<GetCatgoryDetailsViewModel> Handle(GetCategoryDetailsQuery requset, CancellationToken cancellationToken)
        {
            var detailcategory = await _categoryRepository.GetByIdAsync(requset.CategoryId);
            GetCatgoryDetailsViewModel model = new GetCatgoryDetailsViewModel()
            {
                Id = detailcategory.Id,
                EventId = detailcategory.EventId,
                Name = detailcategory.Name,
                Description = detailcategory.Description,
            };
            return model;
        }
    }
}

﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Event.Domain.Entities
{
    public class DImages
    {
        public Guid Id { get; set; }
        public string Image { get; set; }

        public Guid AlbumId { get; set; }
        [ForeignKey(nameof(AlbumId))]
        public virtual DAlbum Album { get; set; }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Event.Presistence.Migrations
{
    public partial class AddRelationBetwenEventAndSource : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "EventId",
                table: "Sources",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "SourceId",
                table: "Events",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sources_EventId",
                table: "Sources",
                column: "EventId");

            migrationBuilder.CreateIndex(
                name: "IX_Events_SourceId",
                table: "Events",
                column: "SourceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Sources_SourceId",
                table: "Events",
                column: "SourceId",
                principalTable: "Sources",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Sources_Events_EventId",
                table: "Sources",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_Sources_SourceId",
                table: "Events");

            migrationBuilder.DropForeignKey(
                name: "FK_Sources_Events_EventId",
                table: "Sources");

            migrationBuilder.DropIndex(
                name: "IX_Sources_EventId",
                table: "Sources");

            migrationBuilder.DropIndex(
                name: "IX_Events_SourceId",
                table: "Events");

            migrationBuilder.DropColumn(
                name: "EventId",
                table: "Sources");

            migrationBuilder.DropColumn(
                name: "SourceId",
                table: "Events");
        }
    }
}

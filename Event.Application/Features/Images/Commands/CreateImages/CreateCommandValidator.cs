﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Images.Commands.CreateImages
{
    public class CreateCommandValidator : AbstractValidator<CreateImagesCommand>
    {
        public CreateCommandValidator()
        {
            RuleFor(x => x.ImageFile)
                .NotEmpty()
                .NotNull();


        }
    }
}

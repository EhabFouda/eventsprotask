﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Category.Commands.UpdateCategory
{
    public class UpdateCommandValidator : AbstractValidator<UpdateCategoryCommand>
    {
        public UpdateCommandValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);

            RuleFor(x => x.Description)
              .NotEmpty()
              .NotNull()
              .MaximumLength(100);


        }
    }
}

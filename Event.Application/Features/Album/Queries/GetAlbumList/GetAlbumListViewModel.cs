﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Album.Queries.GetAlbumList
{
    public class GetAlbumListViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}

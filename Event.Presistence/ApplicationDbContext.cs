﻿using Event.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Presistence
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<DEvent> Events { get; set; }
        public DbSet<DAlbum> Albums { get; set; }
        public DbSet<DSource> Sources { get; set; }
        public DbSet<DCategory> Categories { get; set; }
        public DbSet<DImages> Images { get; set; }
    }
}

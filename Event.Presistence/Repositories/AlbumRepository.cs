﻿using Event.Application.Contracts;
using Event.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Event.Presistence.Repositories
{
    public class AlbumRepository : BaseRepository<DAlbum>, IAlbumRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public AlbumRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<DAlbum>> GetListAlbumsByEventIdAsync(Guid id)
        {
           return (await _dbContext.Albums.Where(x => x.EventId == id).ToListAsync());
        }
    }
}

﻿using Event.Application.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Source.Commands.DeleteSource
{
    public class DeleteSourceCommandHandler : IRequestHandler<DeleteSourceCommand>
    {
        private readonly ISourceRepository _sourceRepository;

        public DeleteSourceCommandHandler(ISourceRepository sourceRepository)
        {
            _sourceRepository = sourceRepository;
        }

        public async Task<Unit> Handle(DeleteSourceCommand request, CancellationToken cancellationToken)
        {
            var source = await _sourceRepository.GetByIdAsync(request.SourceId);
            await _sourceRepository.DeleteAsync(source);
            return Unit.Value;
        }
    }
}


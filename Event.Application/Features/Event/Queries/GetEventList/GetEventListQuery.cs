﻿using Event.Application.Features.Event.Queries.GetEventList;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Event.Queries.GetPostsList
{
    public class GetEventListQuery : IRequest<List<GetEventListViewModel>>
    {

    }
}

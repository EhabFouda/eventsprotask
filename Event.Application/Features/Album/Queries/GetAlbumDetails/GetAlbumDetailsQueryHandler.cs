﻿using AutoMapper;
using Event.Application.Contracts;
using Event.Application.Features.Category.Queries.GetCategoryDetails;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Album.Queries.GetAlbumDetails
{
    public class GetAlbumDetailsQueryHandler : IRequestHandler<GetAlbumDetailsQuery, GetAlbumDetailsViewModel>
    {
        private readonly IAlbumRepository _albumRepository;

        public GetAlbumDetailsQueryHandler(IAlbumRepository albumRepository)
        {
            _albumRepository = albumRepository;
        }

        public async Task<GetAlbumDetailsViewModel> Handle(GetAlbumDetailsQuery requset, CancellationToken cancellationToken)
        {
            var detailalbum = await _albumRepository.GetByIdAsync(requset.AlbumId);
            GetAlbumDetailsViewModel model = new GetAlbumDetailsViewModel()
            {
                Id = detailalbum.Id,
                EventId = detailalbum.EventId,
                Title = detailalbum.Title
            };
            return model;
        }
    }
}

﻿using AutoMapper;
using Event.Application.Common.Helper;
using Event.Application.Features.Album.Commands.CreateAlbum;
using Event.Application.Features.Album.Commands.UpdateAlbum;
using Event.Application.Features.Album.Queries.GetAlbumList;
using Event.Application.Features.Category.Commands.CreateCategory;
using Event.Application.Features.Category.Commands.UpdateCategory;
using Event.Application.Features.Category.Queries.GetCategoryList;
using Event.Application.Features.Event.Commands.CreateEvent;
using Event.Application.Features.Event.Commands.UpdateEvent;
using Event.Application.Features.Event.Queries.GetEventDetails;
using Event.Application.Features.Event.Queries.GetEventList;
using Event.Application.Features.Images.Commands.CreateImages;
using Event.Application.Features.Images.Commands.UpdateImages;
using Event.Application.Features.Images.Queries.GetImagesList;
using Event.Application.Features.Source.Commands.CreateSource;
using Event.Application.Features.Source.Commands.UpdateSource;
using Event.Application.Features.Source.Queries.GetSourceList;
using Event.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Profiles
{
    public class AutoMapperProfile : Profile
    {

        public AutoMapperProfile()
        {
            // Event
            CreateMap<DEvent, GetEventListViewModel>().ReverseMap().ReverseMap();
            CreateMap<DEvent, CreateEventCommand>().ReverseMap().ReverseMap();
            CreateMap<DEvent, UpdateEventCommand>().ReverseMap().ReverseMap();


            // Category
            CreateMap<DCategory, GetCategoryListViewModel>().ReverseMap().ReverseMap();
            CreateMap<DCategory, CreateCategoryCommand>().ReverseMap().ReverseMap();
            CreateMap<DCategory, UpdateCategoryCommand>().ReverseMap().ReverseMap();


            // Album
            CreateMap<DAlbum, GetAlbumListViewModel>().ReverseMap().ReverseMap();
            CreateMap<DAlbum, CreateAlbumCommand>().ReverseMap().ReverseMap();
            CreateMap<DAlbum, UpdateAlbumCommand>().ReverseMap().ReverseMap();


            // Images
            CreateMap<DImages, GetImagesListViewModel>().ReverseMap().ReverseMap();

            CreateMap<DImages, CreateImagesCommand>().ReverseMap().ReverseMap();

            CreateMap<DImages, UpdateImagesCommand>().ReverseMap().ReverseMap();

            // Source
            CreateMap<DSource, GetSourceListViewModel>().ReverseMap().ReverseMap();
            CreateMap<DSource, CreateSourceCommand>().ReverseMap().ReverseMap();
            CreateMap<DSource, UpdateSourceCommand>().ReverseMap().ReverseMap();


        }

    }
}

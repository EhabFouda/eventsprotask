﻿using AutoMapper;
using Event.Application.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Images.Commands.DeleteImages
{
    public class DeleteImagesCommandHandler : IRequestHandler<DeleteImagesCommand>
    {
        private readonly IImageRepository _imagesRepository;

        public DeleteImagesCommandHandler(IImageRepository imagesRepository)
        {
            _imagesRepository = imagesRepository;
        }

        public async Task<Unit> Handle(DeleteImagesCommand request, CancellationToken cancellationToken)
        {
            var image = await _imagesRepository.GetByIdAsync(request.ImageId);
            await _imagesRepository.DeleteAsync(image);
            return Unit.Value;
        }
    }
}

﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Source.Queries.GetSourceList
{
    public class GetSourceListQuery : IRequest<List<GetSourceListViewModel>>
    {

    }
}

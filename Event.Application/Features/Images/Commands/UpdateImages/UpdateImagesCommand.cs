﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Images.Commands.UpdateImages
{
    public class UpdateImagesCommand : IRequest
    {
        public Guid Id { get; set; }
        public Guid AlbumId { get; set; }
        public string Image { get; set; }
        public IFormFile ImageFile { get; set; }
    }
}

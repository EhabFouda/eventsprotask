﻿using AutoMapper;
using Event.Application.Common.Helper;
using Event.Application.Contracts;
using Event.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Event.Commands.CreateEvent
{
    public class CreateEventCommendHandler : IRequestHandler<CreateEventCommand, Guid>
    {
        private readonly IEventRepository _postRepository;
        private readonly IMapper _mapper;
        private readonly IHelper _helper;


        public CreateEventCommendHandler(IEventRepository eventRepository, IMapper mapper, IHelper helper)
        {
            _postRepository = eventRepository;
            _mapper = mapper;
            _helper = helper;
        }

        public async Task<Guid> Handle(CreateEventCommand requset, CancellationToken cancellationToken)
        {
            //DEvent eventt = _mapper.Map<DEvent>(requset);
            DEvent eventt = new DEvent()
            {
                Address = requset.Address,
                Content = requset.Content,
                CoverImg = requset.CoverImg != null ? _helper.Upload(requset.CoverImg) : "",
                NameAr = requset.NameAr,
                NameEn = requset.NameEn,
                StartDate = requset.StartDate,
                EndDate = requset.EndDate,
            };

            CreateCommandValidator validator = new CreateCommandValidator();
            var result = await validator.ValidateAsync(requset);
            if (result.Errors.Any())
            {
                throw new Exception("Event is not valid");
            }

            eventt = await _postRepository.AddAsync(eventt);
            return eventt.Id;
        }
    }
}

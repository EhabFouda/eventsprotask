﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Event.Commands.UpdateEvent
{
    public class UpdateCommandValidator : AbstractValidator<UpdateEventCommand>
    {
        public UpdateCommandValidator()
        {
            RuleFor(x => x.NameAr)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);

            RuleFor(x => x.NameEn)
              .NotEmpty()
              .NotNull()
              .MaximumLength(100);

            RuleFor(x => x.Content)
                .NotEmpty()
                .NotNull();


            RuleFor(x => x.ImageFile).Null();
                

        }
    }
}

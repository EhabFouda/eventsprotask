﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Images.Queries.GetImagesDetails
{
    public class GetImagesDetailsQuery : IRequest<GetImagesDetailsViewModel>
    {
        public Guid ImageId { get; set; }
    }
}

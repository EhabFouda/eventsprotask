﻿using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Images.Commands.CreateImages
{
    public class CreateImagesCommand : IRequest<Guid>
    {
        public Guid AlbumId { get; set; }
        public IFormFile ImageFile { get; set; }

    }
}

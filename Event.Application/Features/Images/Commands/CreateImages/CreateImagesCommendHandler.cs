﻿using AutoMapper;
using Event.Application.Common.Helper;
using Event.Application.Contracts;
using Event.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Images.Commands.CreateImages
{
    public class CreateImagesCommendHandler : IRequestHandler<CreateImagesCommand, Guid>
    {
        private readonly IImageRepository _imagesRepository;
        private readonly IMapper _mapper;
        private readonly IHelper _helper;


        public CreateImagesCommendHandler(IImageRepository albumRepository, IMapper mapper, IHelper helper)
        {
            _imagesRepository = albumRepository;
            _mapper = mapper;
            _helper = helper;

        }

        public async Task<Guid> Handle(CreateImagesCommand requset, CancellationToken cancellationToken)
        {
            //DImages image = _mapper.Map<DImages>(requset);

            DImages image = new DImages()
            {
                Image = requset != null ? _helper.Upload(requset.ImageFile) : "",
                AlbumId = requset.AlbumId,
            };

            CreateCommandValidator validator = new CreateCommandValidator();
            var result = await validator.ValidateAsync(requset);
            if (result.Errors.Any())
            {
                throw new Exception("Image is not valid");
            }

            image = await _imagesRepository.AddAsync(image);
            return image.Id;
        }
    }
}
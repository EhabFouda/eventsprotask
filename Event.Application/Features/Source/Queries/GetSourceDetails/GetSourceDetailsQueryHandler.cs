﻿using Event.Application.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Source.Queries.GetSourceDetails
{
    public class GetSourceDetailsQueryHandler : IRequestHandler<GetSourceDetailsQuery, GetSourceDetailsViewModel>
    {
        private readonly ISourceRepository _sourceRepository;

        public GetSourceDetailsQueryHandler(ISourceRepository eventRepository)
        {
            _sourceRepository = eventRepository;
        }

        public async Task<GetSourceDetailsViewModel> Handle(GetSourceDetailsQuery requset, CancellationToken cancellationToken)
        {
            var detailevent = await _sourceRepository.GetByIdAsync(requset.SourceId);
            GetSourceDetailsViewModel model = new GetSourceDetailsViewModel()
            {
                Id = detailevent.Id,
                EventId = detailevent.EventId,
                Name = detailevent.Name,
                Description = detailevent.Description,
            };
            return model;
        }
    }
}

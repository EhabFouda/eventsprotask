﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Event.Commands.CreateEvent
{
    public class CreateCommandValidator : AbstractValidator<CreateEventCommand>
    {
        public CreateCommandValidator()
        {
            RuleFor(x => x.NameAr)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);

              RuleFor(x => x.NameEn)
                .NotEmpty()
                .NotNull()
                .MaximumLength(100);

            RuleFor(x => x.Content)
                .NotEmpty()
                .NotNull();
        }
    }
}

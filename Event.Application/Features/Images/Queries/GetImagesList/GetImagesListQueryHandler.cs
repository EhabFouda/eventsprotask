﻿using AutoMapper;
using Event.Application.Contracts;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event.Application.Features.Images.Queries.GetImagesList
{
    public class GetImagesListQueryHandler : IRequestHandler<GetImagesListQuery, List<GetImagesListViewModel>>
    {
        private readonly IImageRepository _imagesRepository;
        private readonly IMapper _mapper;

        public GetImagesListQueryHandler(IImageRepository imagesRepository, IMapper mapper)
        {
            _imagesRepository = imagesRepository;
            _mapper = mapper;
        }

        public async Task<List<GetImagesListViewModel>> Handle(GetImagesListQuery request, CancellationToken cancellationToken)
        {
            var allimages = await _imagesRepository.GetListImagesByAlbumIdAsync(request.AlbumId);
            return _mapper.Map<List<GetImagesListViewModel>>(allimages);
        }
    }
}

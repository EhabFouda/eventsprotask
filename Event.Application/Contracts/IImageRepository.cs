﻿using Event.Domain.Entities;

namespace Event.Application.Contracts
{
    public interface IImageRepository : IAsyncRepository<DImages>
    {
        Task<List<DImages>> GetListImagesByAlbumIdAsync(Guid id);


    }
}
